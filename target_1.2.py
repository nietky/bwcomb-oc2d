# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

from phd_modules import *
import os.path

import logging
logging.basicConfig(level="DEBUG")

# <codecell>

edipath = r"z:\bw12\mtpro\gridcoords"
edifns = ["bspr.edi", "bspre1.edi"]
distances = [1000, 1100]
data = []
for i, edifn in enumerate(edifns):
    freqs, Zs, Zes = mt.read_edi(os.path.join(edipath, edifn))
    freqs, Zs = mt.linear_interp(freqs, Zs, np.logspace(np.log10(0.2), np.log10(100), 15))
    res = mt.appres(freqs, Zs)
    phase = mt.phase(Zs)
    res_e = mt.set_error_floor(res, floor=0.05)
    phase_e = mt.set_error_floor(phase, floor=0.05)
    d = {"name": edifn.replace(".edi", ""),
         "distance": distances[i],
         "freqs": freqs,
         "res_te": res[:, 0, 1],
         "res_tm": res[:, 1, 0],
         "phase_te": phase[:, 0, 1],
         "phase_tm": phase[:, 1, 0],
         "res_te_e": res_e[:, 0, 1],
         "res_tm_e": res_e[:, 1, 0],
         "phase_te_e": phase_e[:, 0, 1],
         "phase_tm_e": phase_e[:, 1, 0]}
    data.append(d)

# <codecell>

invpath = r".\target_1.2"
datafn = "bspr+bspre1.data.txt"

# <codecell>

oc2d.write_data(data, os.path.join(invpath, datafn), title="bspr and bspre1 (ensemble Type A)")

# <codecell>

reload(oc2d)
oc2d.run(os.path.join(invpath, "startup"))

# <codecell>

reload(oc2d)
iterfns = oc2d.get_iter_fns(invpath)
nums = oc2d.get_iter_nums(invpath)
print nums

# <codecell>

%pylab inline
reload(oc2d)
r = oc2d.read_resp(iterindex=-1)

# <codecell>

site = "bspre1"
obs = {"lw": 0.4}
fwd = {"lw": 1}
te = {"color": "b"}
tm = {"color": "g"}

fig = plt.figure(figsize=(12, 3))
axl = fig.add_subplot(121)
axr = fig.add_subplot(122)
axl.loglog(r["freqs"], r[site]["res_te"], **oc2d.merge_dicts(obs, te, {"label": "obs TE"}))
axl.loglog(r["freqs"], r[site]["fwd_res_te"], **oc2d.merge_dicts(fwd, te, {"label": "fwd TE"}))
axl.loglog(r["freqs"], r[site]["res_tm"], **oc2d.merge_dicts(obs, tm, {"label": "obs TM"}))
axl.loglog(r["freqs"], r[site]["fwd_res_tm"], **oc2d.merge_dicts(fwd, tm, {"label": "fwd TM"}))
axr.semilogx(r["freqs"], r[site]["phase_te"], **oc2d.merge_dicts(obs, te, {"label": "obs TE"}))
axr.semilogx(r["freqs"], r[site]["fwd_phase_te"], **oc2d.merge_dicts(fwd, te, {"label": "fwd TE"}))
axr.semilogx(r["freqs"], r[site]["phase_tm"], **oc2d.merge_dicts(obs, tm, {"label": "obs TM"}))
axr.semilogx(r["freqs"], r[site]["fwd_phase_tm"], **oc2d.merge_dicts(fwd, tm, {"label": "fwd TM"}))
axl.set_xlim(*axl.get_xlim()[::-1])
axr.set_xlim(*axr.get_xlim()[::-1])
axr.legend(loc="best")

# <codecell>

%pylab inline
reload(oc2d)
fig = plt.figure(1)#, figsize=(15, 12))
#ax = fig.add_subplot(111)
ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])
ax_cbar = fig.add_axes([0.92, 0.1, 0.03, 0.8])
pm = oc2d.PlotModel()
pm.plot(iterfns[nums[-1]], model_ax=ax, cbar_ax=ax_cbar, cbar_orient="vertical")
pm.model_ax.set_ylim(500, 0)
pm.model_ax.set_xlim(700, 1300)
#pm.set_model_blocks(ls="dotted", lw=0.5)
pm.set_mesh_visibility(False)
pm.set_model_visibility(0)

